plugins {
    kotlin("jvm") apply false
    `kotlin-dsl`
    java
    `java-gradle-plugin`
    id("net.nemerosa.versioning")
}

allprojects {
    repositories {
        mavenCentral()
        maven(url = "https://jitpack.io")
        gradlePluginPortal()
    }
}
