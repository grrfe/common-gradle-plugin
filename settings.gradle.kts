rootProject.name = "common-gradle-plugin"

pluginManagement {
    repositories {
        mavenCentral()
        mavenLocal()
        maven(url = "https://jitpack.io")
        gradlePluginPortal()
    }

    plugins {
        kotlin("jvm") version "1.9.20"
        id("net.nemerosa.versioning") version "3.0.0"
        id("com.github.johnrengelman.shadow") version "8.1.1"
        id("org.gradle.maven-publish")
    }
}

plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version("0.5.0")
}

include("common")
include("android", "app", "library")
