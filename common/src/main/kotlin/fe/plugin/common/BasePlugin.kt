package fe.plugin.common

import com.github.jengelman.gradle.plugins.shadow.ShadowPlugin
import fe.plugin.common.extension.add
import net.nemerosa.versioning.VersioningExtension
import net.nemerosa.versioning.VersioningPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.kotlin.dsl.*
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper
import kotlin.reflect.KClass

abstract class BasePlugin<T : BaseConfig, I : Any>(
    private val extensionName: String,
    private val configClass: KClass<T>,
    private val additionalPlugins: Set<KClass<out Plugin<Project>>>
) : Plugin<Project> {

    companion object {
        val basePlugins = listOf(
            KotlinPluginWrapper::class,
            VersioningPlugin::class,
            ShadowPlugin::class,
        )

//        @JvmInline
//        value class Group(val group: String)

//        data class Artifact(val group: Group, val artifact: String) {
//            fun toSpec(): Spec<ResolvedDependency> {
//                return Specs.convertClosureToSpec(KotlinClosure1<ResolvedDependency, Boolean>({
//                    moduleGroup == group.group && moduleName == artifact
//                }))
//            }
//
//            fun toDependencyNotation(): String {
//                return "${group.group}:$artifact"
//            }
//        }

        val repositories = listOf<RepositoryHandler.() -> Unit>(
            { mavenCentral() },
            { maven { setUrl("https://jitpack.io") } }
        )

        val JAR_EXCLUDES = setOf("**/*.idea", "**/*.git", "**/*.gradle", "**/venv")

        private const val KOTLIN_GROUP = "org.jetbrains.kotlin"
        private val KOTLIN_TEST_DEPENDENCY = PluginDependency.AnyVersion(KOTLIN_GROUP, "kotlin-test")

        val KOTLIN_STD_LIB_DEPENDENCIES = with("kotlin-stdlib") {
            setOf("-jdk7", "-jdk8", "").map { suffix -> PluginDependency.AnyVersion(KOTLIN_GROUP, this + suffix) }
        }
    }

    override fun apply(project: Project) {
        (basePlugins + additionalPlugins).forEach { project.pluginManager.apply(it) }
        repositories.forEach { it(project.repositories) }

        val config = project.extensions.create(extensionName, configClass, project.objects)

        val versioning = project.extensions.getByType<VersioningExtension>()
        val kotlin = project.extensions.getByType<KotlinJvmProjectExtension>()

        val shadow by project.configurations
        shadow.isVisible = false

        val testImplementation by project.configurations

        val input = applyImpl(project, config)

        project.afterEvaluate {
            config.disallowChanges()

            if (config.group.orNull == null) {
                logger.warn("Library 'group' has not been set!")
                return@afterEvaluate
            }

            project.group = config.group.get()
            project.version = config.versionStrategy.get().getVersion(versioning)

            if (config.includeKotlinTest.get()) {
                project.dependencies.add(testImplementation, KOTLIN_TEST_DEPENDENCY)
            }

            kotlin.jvmToolchain(config.jvm.get())
            afterEvaluate(project, config, input)
        }
    }


    abstract fun applyImpl(project: Project, config: T): I

    open fun afterEvaluate(project: Project, config: T, input: I) {}
}
