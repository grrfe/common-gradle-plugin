package fe.plugin.common

import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.HasConfigurableValue
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.kotlin.dsl.listProperty
import org.gradle.kotlin.dsl.newInstance
import org.gradle.kotlin.dsl.property

abstract class PropertyTracker(val objectFactory: ObjectFactory) {
    private val properties = mutableListOf<HasConfigurableValue>()
    private val trackers = mutableListOf<PropertyTracker>()

    private var changesAllowed = true

    private fun check() {
        if (!changesAllowed) error("Config ${javaClass.simpleName} has already been configured!")
    }

    fun <T : HasConfigurableValue> add(property: T): T {
        check()
        properties.add(property)
        return property
    }

    fun <T : PropertyTracker> add(tracker: T): T {
        check()
        trackers.add(tracker)
        return tracker
    }

    fun disallowChanges() {
        check()
        trackers.forEach { it.disallowChanges() }
        properties.forEach { it.disallowChanges() }
        changesAllowed = false
    }

    inline fun <reified T : PropertyTracker> newInstance(): T {
        return add(objectFactory.newInstance<T>(objectFactory))
    }

    inline fun <reified T> property(value: T): Property<T> {
        return add(objectFactory.property<T>().convention(value))
    }

    inline fun <reified T> property(): Property<T> {
        return add(objectFactory.property(T::class.java))
    }

    inline fun <reified T> listProperty(elements: List<T>): ListProperty<T> {
        return add(objectFactory.listProperty<T>().convention(elements))
    }

    inline fun <reified T> listProperty(): ListProperty<T> {
        return add(objectFactory.listProperty(T::class.java))
    }

    inline fun <reified K, reified V> mapProperty(): Provider<out MutableMap<K, V>> {
        return add(objectFactory.mapProperty(K::class.java, V::class.java))
    }
}
