package fe.plugin.common.version

import net.nemerosa.versioning.VersioningExtension

interface VersionStrategy {
    fun getVersion(versioningExtension: VersioningExtension): String

    companion object {
        val default = VersionFromTagOrFull
    }
}

object VersionFromTagOrFull : VersionStrategy {
    override fun getVersion(versioningExtension: VersioningExtension): String {
        return versioningExtension.info.tag ?: versioningExtension.info.full ?: "0.0.0"
    }
}
