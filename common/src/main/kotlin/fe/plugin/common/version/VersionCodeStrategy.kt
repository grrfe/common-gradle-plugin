package fe.plugin.common.version


import fe.plugin.common.groovy.KotlinClosure2
import fe.plugin.common.groovy.KotlinClosure4
import net.nemerosa.versioning.ReleaseInfo
import net.nemerosa.versioning.SCMInfo
import net.nemerosa.versioning.VersioningExtension

interface VersionCodeStrategy {
    fun getVersion(versioning: VersioningExtension): Int

    companion object {
        val default = VersionCodeFromTagOrUnixSeconds
    }
}

object VersionCodeFromTagOrUnixSeconds : VersionCodeStrategy {
    override fun getVersion(versioning: VersioningExtension): Int {
        versioning.releaseMode = KotlinClosure4 { _: String?, _: String?, currentTag: String?, _: VersioningExtension ->
            currentTag
        }

        versioning.releaseParser = KotlinClosure2 { info: SCMInfo, _: String ->
            ReleaseInfo("release", info.tag)
        }

        return versioning.info.tag?.let {
            versioning.info.versionNumber.versionCode
        } ?: (System.currentTimeMillis() / 1000).toInt()
    }
}


