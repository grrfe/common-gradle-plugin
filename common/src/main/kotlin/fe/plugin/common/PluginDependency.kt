package fe.plugin.common

import fe.plugin.common.groovy.spec1
import org.gradle.api.artifacts.ResolvedDependency
import org.gradle.api.specs.Spec

sealed class PluginDependency(val group: String, val name: String) {
    class AnyVersion(group: String, name: String) : PluginDependency(group, name) {
        val notationAnyVersion = "$group:$name"

        fun asSpec(): Spec<ResolvedDependency> {
            return spec1 { this@AnyVersion.group == moduleGroup && this@AnyVersion.name == moduleName }
        }
    }

    class WithVersion(group: String, name: String, version: String) : PluginDependency(group, name) {
        val notation = "$group:$name:$version"
    }
}
