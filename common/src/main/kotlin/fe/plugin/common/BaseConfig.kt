package fe.plugin.common

import fe.plugin.common.version.VersionStrategy
import org.gradle.api.model.ObjectFactory

abstract class BaseConfig(objectFactory: ObjectFactory) : PropertyTracker(objectFactory) {
    val group = property<String>()
    val jvm = property(11)

    val versionStrategy = property<VersionStrategy>(VersionStrategy.default)
    val includeKotlinTest = property(true)
}

