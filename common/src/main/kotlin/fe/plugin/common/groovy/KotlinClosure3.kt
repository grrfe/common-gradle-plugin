package fe.plugin.common.groovy

import groovy.lang.Closure

class KotlinClosure3<in T : Any?, in U : Any?, in V : Any?, R : Any>(
    val function: (T, U, V) -> R?,
    owner: Any? = null,
    thisObject: Any? = null
) : Closure<R?>(owner, thisObject) {

    @Suppress("unused") // to be called dynamically by Groovy
    fun doCall(t: T, u: U, v: V): R? = function(t, u, v)
}

