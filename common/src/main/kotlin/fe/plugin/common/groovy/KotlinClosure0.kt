package fe.plugin.common.groovy

import groovy.lang.Closure

open class KotlinClosure0<V : Any>(
    val function: () -> V?,
    owner: Any? = null,
    thisObject: Any? = null
) : Closure<V?>(owner, thisObject) {

    @Suppress("unused") // to be called dynamically by Groovy
    fun doCall(): V? = function()
}
