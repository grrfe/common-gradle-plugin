package fe.plugin.common.groovy

import groovy.lang.Closure

class KotlinClosure2<in T : Any?, in U : Any?, V : Any>(
    owner: Any? = null,
    thisObject: Any? = null,
    val function: (T, U) -> V?,
) : Closure<V?>(owner, thisObject) {

    @Suppress("unused") // to be called dynamically by Groovy
    fun doCall(t: T, u: U): V? = function(t, u)
}

