package fe.plugin.common.groovy

import org.gradle.api.specs.Spec
import org.gradle.api.specs.Specs
import org.gradle.kotlin.dsl.KotlinClosure1

fun <T, V : Any> spec1(fn: T.() -> V?): Spec<T> {
    return Specs.convertClosureToSpec(KotlinClosure1(function = fn))
}

//fun <T, V : Any> spec1(t: T,  fn: (T) -> V?): Spec<T> {
//    return Specs.convertClosureToSpec(KotlinClosure1(function = fn))
//}
