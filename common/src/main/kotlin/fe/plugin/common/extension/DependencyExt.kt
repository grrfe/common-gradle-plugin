package fe.plugin.common.extension

import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.ResolvedDependency
import org.gradle.api.attributes.Attribute

fun <T> ResolvedDependency.getAttribute(attr: Attribute<T>): T? {
    if (this !is ExternalModuleDependency) return null
    return attributes.getAttribute(attr)
}

fun <T> Dependency.getAttribute(attr: Attribute<T>): T? {
    if (this !is ExternalModuleDependency) return null
    return attributes.getAttribute(attr)
}

