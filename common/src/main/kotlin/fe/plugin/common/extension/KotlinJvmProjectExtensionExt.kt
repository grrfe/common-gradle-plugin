package fe.plugin.common.extension

import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.file.SourceDirectorySet
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

val NamedDomainObjectContainer<KotlinSourceSet>.kotlinMain: SourceDirectorySet
    get() = getByName("main").kotlin
