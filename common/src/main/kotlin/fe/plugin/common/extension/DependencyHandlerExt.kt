package fe.plugin.common.extension

import fe.plugin.common.PluginDependency
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler

fun DependencyHandler.add(configuration: Configuration, dependency: PluginDependency.WithVersion): Dependency? {
    return add(configuration.name, dependency.notation)
}

fun DependencyHandler.add(configuration: Configuration, dependency: PluginDependency.AnyVersion): Dependency? {
    return add(configuration.name, dependency.notationAnyVersion)
}

