package fe.plugin.common.extension

import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.SourceSetContainer


val SourceSetContainer.javaMain: SourceDirectorySet
    get() = getByName("main").java
