package fe.plugin.common.dependency

import de.fayard.refreshVersions.core.DependencyGroup

object HttpKt : DependencyGroup(group = "com.gitlab.grrfe.httpkt") {
    val core = module("core")
    val gsonExt = module("ext-gson")
    val jsoupExt = module("ext-jsoup")
}

