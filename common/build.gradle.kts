plugins {
    kotlin("jvm")
    `maven-publish`
    `kotlin-dsl`
    java
    `java-gradle-plugin`
    id("net.nemerosa.versioning")
}

group = "fe.plugin.common"
//version = versioning.

kotlin {
    jvmToolchain(17)
}

dependencies {
    api("net.nemerosa.versioning:net.nemerosa.versioning.gradle.plugin:3.0.0")
    api("com.github.johnrengelman.shadow:com.github.johnrengelman.shadow.gradle.plugin:8.1.1")
    api("org.jetbrains.kotlin.jvm:org.jetbrains.kotlin.jvm.gradle.plugin:1.9.20")
    api("de.fayard.refreshVersions:refreshVersions-core:0.60.1")
//    implementation("org.ajoberstar.grgit:grgit-core:4.1.1")
//    implementation("org.eclipse.jgit:org.eclipse.jgit.ui:6.8.0.202311291450-r")
//    implementation("org.eclipse.jgit:org.eclipse.jgit:6.8.0.202311291450-r")
//    implementation("org.tmatesoft.svnkit:svnkit:1.10.6")
//    implementation("org.gradle.kotlin:gradle-kotlin-dsl-plugins:4.2.1")
//
}

//tasks.compileGroovy {
//    classpath = sourceSets.main.get().compileClasspath
//}
//
//tasks.compileKotlin {
//    libraries.from(files(sourceSets.main.get().groovy.classesDirectory))
//}


//val compileKotlin: KotlinCompile by tasks
//
//compileKotlin.kotlinOptions {
//    languageVersion = "1.9"
//}
//val compileKotlin: KotlinCompile by tasks
//compileKotlin.kotlinOptions {
//    languageVersion = "1.9"
//}
