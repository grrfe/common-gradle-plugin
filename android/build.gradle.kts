plugins {
    kotlin("jvm")
//    id("net.nemerosa.versioning") version "3.0.0"
}

group = "fe.plugin.android"
//version = versioning.info.tag ?: versioning.info.full
version = "testing"

//gradlePlugin {
//    plugins {
//        create("android") {
//            id = "fe.gradle.plugin.android"
//            implementationClass = "fe.gradle.plugin.android.AndroidPlugin"
//        }
//    }
//}

kotlin {
    jvmToolchain(17)
}

dependencies {
    compileOnly(project(":common"))
    implementation("net.nemerosa.versioning:net.nemerosa.versioning.gradle.plugin:3.0.0")
}

//tasks.getByName<Jar>("jar") {
//    from(project(":common").sourceSets.main.get().output)
//}
