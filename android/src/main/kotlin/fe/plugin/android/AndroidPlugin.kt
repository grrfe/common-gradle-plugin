package fe.plugin.android

import net.nemerosa.versioning.VersioningExtension
import org.gradle.api.Plugin
import org.gradle.api.Project

class AndroidPlugin : Plugin<Project> {
    // TODO: fix Gradle 8.1+ (eval git later)
    override fun apply(project: Project) {
        val versioningExtension = VersioningExtension(project)

        project.extensions.create(
            "versionInfo",
            VersionInfo::class.java,
            project,
            versioningExtension,
        )
    }
}
