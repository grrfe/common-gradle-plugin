package fe.plugin.android

import fe.plugin.common.version.VersionCodeStrategy
import fe.plugin.common.version.VersionStrategy
import net.nemerosa.versioning.VersioningExtension
import org.gradle.api.Project

open class VersionInfo(val project: Project, private val versioningExtension: VersioningExtension) {
    fun getVersion(versionStrategy: VersionStrategy = VersionStrategy.default): String {
        return versionStrategy.getVersion(versioningExtension)
    }

    fun getVersionCode(versionStrategy: VersionCodeStrategy = VersionCodeStrategy.default): Int {
        return versionStrategy.getVersion(versioningExtension)
    }
}
