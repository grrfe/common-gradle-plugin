plugins {
    kotlin("jvm")
    `maven-publish`
    `kotlin-dsl`
    java
    `java-gradle-plugin`
    id("net.nemerosa.versioning")
}

group = "fe.plugin.app"
version = versioning.info.tag ?: versioning.info.full

kotlin {
    jvmToolchain(17)
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

//open class Test123 {
//    var minimize = false
//}

//val additionalProcessingExt =
//    fun ModuleDependency.(introspect: Boolean) {
//        attributes {
//            attribute(Attribute.of("my.project.processing", Boolean::class.javaObjectType), introspect)
//        }
//    }
//
//dependencies.extensions.add(
//    typeOf<@ExtensionFunctionType ModuleDependency.(Boolean) -> Unit>(),
//    "additionalProcessing",
//    additionalProcessingExt,
//)
//
///**
// * Retrieves the [additionalProcessing][Function2<ModuleDependency, Boolean, kotlin.Unit>] extension.
// */
//val DependencyHandler.additionalProcessing: @ExtensionFunctionType Function2<ModuleDependency, Boolean, Unit>
//    get() = (this as ExtensionAware).extensions.getByName("additionalProcessing") as Function2<ModuleDependency, Boolean, Unit>
//
//val bundled2 =
//    (project.dependencies as ExtensionAware).extensions.create("tester", Test123::class.java, project.objects)

//fun DependencyHandler.bundled2(notation: String, action: Action<Test123>) {
//    val dependency = add("bundled", notation)
//
//
//    dependency!!.extra
//    dependency.attribu
//}


gradlePlugin {
    plugins {
        create("app") {
            id = "com.gitlab.grrfe.common-gradle-plugin"
            implementationClass = "fe.plugin.app.AppPlugin"
        }
    }
}

dependencies {
    compileOnly(project(":common"))
    implementation("com.squareup:javapoet:1.13.0")
    api("net.nemerosa.versioning:net.nemerosa.versioning.gradle.plugin:3.0.0")
    api("com.github.johnrengelman.shadow:com.github.johnrengelman.shadow.gradle.plugin:8.1.1")
    api("org.jetbrains.kotlin.jvm:org.jetbrains.kotlin.jvm.gradle.plugin:1.9.20")
    api("de.fayard.refreshVersions:refreshVersions-core:0.60.1")
}

tasks.named<Jar>("jar") {
    from(project(":common").sourceSets.main.get().output)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

tasks.withType<GenerateModuleMetadata> {
    enabled = false
}


//tasks.withType<Jar> {
//    from(project(":common").sourceSets.main.get().output)
//    from(configurations.runtimeClasspath.get().map {
//        if (it.isDirectory) it else zipTree(it)
//    })
//
//    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
//}


//publishing.publications {
//    afterEvaluate {
//        getByName<MavenPublication>("pluginMaven").pom.withXml {
//            val pomNode = asNode()
//            (pomNode.get("dependencies") as NodeList).forEach {
//                pomNode.remove(it as Node)
//            }
//        }
//    }
//}
