package fe.plugin.app

import com.github.jengelman.gradle.plugins.shadow.ShadowJavaPlugin
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import fe.plugin.app.buildinfo.BuildInfoClassGenerator
import fe.plugin.common.BasePlugin
import fe.plugin.common.extension.javaMain
import fe.plugin.common.extension.kotlinMain
import net.nemerosa.versioning.VersioningExtension
import org.gradle.api.Project
import org.gradle.api.plugins.ApplicationPlugin
import org.gradle.api.plugins.JavaApplication
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.kotlin.dsl.getByType
import org.gradle.kotlin.dsl.getValue
import org.gradle.kotlin.dsl.named
import org.gradle.kotlin.dsl.provideDelegate
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension
import java.io.File
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set

class AppPlugin : BasePlugin<AppConfig, Unit>("application", AppConfig::class, setOf(ApplicationPlugin::class)) {
    companion object {
        private const val MANIFEST_MAIN_CLASS_KEY = "Main-Class"
        private const val GENERATE_BUILD_INFO_TASK = "generateBuildInfo"
        private const val SEMVER_DEPENDENCY = "com.github.zafarkhaja:java-semver:0.9.0"
    }

    override fun applyImpl(project: Project, config: AppConfig) {

    }

    override fun afterEvaluate(project: Project, config: AppConfig, input: Unit) {
        val application = project.extensions.getByType<JavaApplication>()
        application.mainClass.set(config.mainClass)

        project.tasks.named<ShadowJar>(ShadowJavaPlugin.SHADOW_JAR_TASK_NAME) {
            archiveClassifier.set("")
            exclude(JAR_EXCLUDES)

            duplicatesStrategy = config.jarConfig.duplicateStrategy.get()

            if (config.jarConfig.minimize.get()) {
                minimize()
            }

            manifest {
                attributes[MANIFEST_MAIN_CLASS_KEY] = config.mainClass.get()
                config.jarConfig.additionalManifestAttributes.get().forEach { (key, value) ->
                    if (attributes[key] == null) attributes[key] = value
                }
            }
        }

        if (config.buildInfoConfig.enabled.get()) {
            val generatedDir = project.layout.buildDirectory.file(config.buildInfoConfig.srcDir)
            val versioning = project.extensions.getByType<VersioningExtension>()
            val implementation by project.configurations

            implementation.dependencies.add(
                project.dependencies.create(SEMVER_DEPENDENCY)
            )

            project.tasks.register(GENERATE_BUILD_INFO_TASK) {
                val classes by project.tasks
                classes.dependsOn(this)

                val dir = generatedDir.get().asFile
                dir.mkdirs()

                val javaFile = BuildInfoClassGenerator.build(
                    versioning.info,
                    config.buildInfoConfig.config.get(),
                    config.buildInfoConfig.packageName.get()
                )

                File(dir, "${javaFile.typeSpec.name}.java").delete()
                javaFile.writeToFile(dir)

                group = "build"
            }

            val java = project.extensions.getByType<JavaPluginExtension>()
            val kotlin = project.extensions.getByType<KotlinJvmProjectExtension>()

            setOf(kotlin.sourceSets.kotlinMain, java.sourceSets.javaMain).forEach {
                it.srcDir(generatedDir)
            }
        }
    }
}
