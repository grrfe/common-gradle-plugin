package fe.plugin.app

import fe.plugin.app.buildinfo.BuildInfoClassConfig
import fe.plugin.app.buildinfo.DefaultBuildInfoClassConfig
import fe.plugin.common.BaseConfig
import fe.plugin.common.PropertyTracker
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.model.ObjectFactory
import org.gradle.kotlin.dsl.getByType
import javax.inject.Inject

open class AppConfig @Inject constructor(objectFactory: ObjectFactory) : BaseConfig(objectFactory) {
    val mainClass = property<String>()

    val jarConfig = newInstance<AppJarConfig>()

    fun jarConfig(action: Action<AppJarConfig>) {
        action.execute(jarConfig)
    }

    val buildInfoConfig = newInstance<BuildInfoConfig>()

    fun buildInfo(action: Action<BuildInfoConfig>) {
        action.execute(buildInfoConfig)
    }

    companion object {
        fun Project.application(group: String, mainClass: String, action: Action<AppConfig>): AppConfig {
            val config = extensions.getByType<AppConfig>()
            config.group.set(group)
            config.mainClass.set(mainClass)

            action.execute(config)
            return config
        }
    }
}

open class AppJarConfig @Inject constructor(objectFactory: ObjectFactory) : PropertyTracker(objectFactory) {
    val minimize = property(false)
    val duplicateStrategy = property(DuplicatesStrategy.EXCLUDE)
    val additionalManifestAttributes = mapProperty<String, Any>()
}

open class BuildInfoConfig @Inject constructor(objectFactory: ObjectFactory) : PropertyTracker(objectFactory) {
    val enabled = property(true)
    val packageName = property("build")
    val srcDir = property("generated/buildinfo")
    val config = property<BuildInfoClassConfig>(DefaultBuildInfoClassConfig)
}
