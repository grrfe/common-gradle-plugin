package fe.plugin.app.buildinfo

import com.squareup.javapoet.FieldSpec
import net.nemerosa.versioning.VersionInfo
import kotlin.properties.PropertyDelegateProvider
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KClass

typealias GetVersionProperty<T> = (VersionInfo) -> T

abstract class BuildInfoClassConfig {
    private val properties = mutableMapOf<String, BuildInfoClassField>()

    private fun <T : BuildInfoClassField> addIfNotDuplicate(name: String, field: T): T {
        if (properties[name] != null) error("Duplicate property added!")
        properties[name] = field
        return field
    }

    fun addVersionProperty(
        name: String,
        getter: GetVersionProperty<*>,
        clazz: KClass<out Any>
    ): VersionBuildInfoClassField {
        return addIfNotDuplicate(name, VersionBuildInfoClassField(name, clazz, getter))
    }

    fun addCustomProperty(name: String, value: Any): CustomBuildInfoField {
        return addIfNotDuplicate(name, CustomBuildInfoField(name, value))
    }

    fun addSemverProperty(name: String): SemverBuildInfoField {
        return addIfNotDuplicate(name, SemverBuildInfoField(name))
    }

    fun addBuildTimeProperty(name: String): BuildTimeInfoField {
        return addIfNotDuplicate(name, BuildTimeInfoField(name))
    }

    fun build(versionInfo: VersionInfo): List<FieldSpec> {
        return properties.map { (name, versionFileField) ->
            versionFileField.createField(versionInfo)
        }
    }

    fun semverProperty(): PropertyDelegateProvider<BuildInfoClassConfig, ReadOnlyProperty<Any, SemverBuildInfoField>> {
        return PropertyDelegateProvider { fileConfig: BuildInfoClassConfig, property ->
            val field = fileConfig.addSemverProperty(property.name)
            ReadOnlyProperty { _, _ -> field }
        }
    }

    fun buildTimeProperty(): PropertyDelegateProvider<BuildInfoClassConfig, ReadOnlyProperty<Any, BuildTimeInfoField>> {
        return PropertyDelegateProvider { fileConfig: BuildInfoClassConfig, property ->
            val field = fileConfig.addBuildTimeProperty(property.name)
            ReadOnlyProperty { _, _ -> field }
        }
    }

    fun customProperty(
        value: String
    ): PropertyDelegateProvider<BuildInfoClassConfig, ReadOnlyProperty<Any, CustomBuildInfoField>> {
        return PropertyDelegateProvider { fileConfig: BuildInfoClassConfig, property ->
            val field = fileConfig.addCustomProperty(property.name, value)
            ReadOnlyProperty { _, _ -> field }
        }
    }

    inline fun <reified T : Any> versionInfoProperty(
        noinline getter: GetVersionProperty<T>
    ): PropertyDelegateProvider<BuildInfoClassConfig, ReadOnlyProperty<Any, VersionBuildInfoClassField>> {
        return PropertyDelegateProvider { fileConfig: BuildInfoClassConfig, property ->
            val field = fileConfig.addVersionProperty(property.name, getter, T::class)
            ReadOnlyProperty { _, _ -> field }
        }
    }
}
