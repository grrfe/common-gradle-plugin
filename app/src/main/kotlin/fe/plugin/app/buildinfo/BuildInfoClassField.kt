package fe.plugin.app.buildinfo

import com.squareup.javapoet.ClassName
import com.squareup.javapoet.FieldSpec
import net.nemerosa.versioning.VersionInfo
import java.time.LocalDateTime
import kotlin.reflect.KClass

abstract class BuildInfoClassField(val name: String) {
    abstract fun createField(versionInfo: VersionInfo): FieldSpec
}

class VersionBuildInfoClassField(
    name: String,
    private val clazz: KClass<out Any>,
    val getter: GetVersionProperty<*>
) : BuildInfoClassField(name) {
    override fun createField(versionInfo: VersionInfo): FieldSpec {
        val format = if (clazz == String::class) "\$S" else "\$L"
        return BuildInfoClassGenerator.createConstantField(
            clazz,
            name,
            value = arrayOf(getter(versionInfo)),
            format = format
        )
    }
}

class CustomBuildInfoField(name: String, private val value: Any) : BuildInfoClassField(name) {
    override fun createField(versionInfo: VersionInfo): FieldSpec {
        return BuildInfoClassGenerator.createConstantField(value::class, name, value)
    }
}

class SemverBuildInfoField(name: String) : BuildInfoClassField(name) {
    companion object {
        val semverClass: ClassName = ClassName.get("com.github.zafarkhaja.semver", "Version")
    }

    override fun createField(versionInfo: VersionInfo): FieldSpec {
        return BuildInfoClassGenerator.createConstantField(
            semverClass,
            name,
            format = "\$T.valueOf(\$S)",
            value = arrayOf(semverClass, versionInfo.tag ?: "0.0.0"),
        )
    }
}

class BuildTimeInfoField(name: String) : BuildInfoClassField(name) {
    override fun createField(versionInfo: VersionInfo): FieldSpec {
        return BuildInfoClassGenerator.createConstantField(
            LocalDateTime::class,
            name,
            format = "\$T.parse(\$S)",
            value = arrayOf(LocalDateTime::class.java, LocalDateTime.now().toString()),
        )
    }
}
