package fe.plugin.app.buildinfo

import net.nemerosa.versioning.VersionInfo

object DefaultBuildInfoClassConfig : BuildInfoClassConfig() {
    val SEMVER by semverProperty()
    val BUILT_AT by buildTimeProperty()
    val FULL by versionInfoProperty(VersionInfo::getFull)
}



