package fe.plugin.app.buildinfo

import com.squareup.javapoet.FieldSpec
import com.squareup.javapoet.JavaFile
import com.squareup.javapoet.TypeName
import com.squareup.javapoet.TypeSpec
import net.nemerosa.versioning.VersionInfo
import javax.lang.model.element.Modifier
import kotlin.reflect.KClass

object BuildInfoClassGenerator {

    fun createConstantField(typeName: TypeName, name: String, vararg value: Any?, format: String = "\$L"): FieldSpec {
        return FieldSpec.builder(typeName, name).constantField(value = value, format = format)
    }

    fun createConstantField(clazz: KClass<*>, name: String, vararg value: Any?, format: String = "\$L"): FieldSpec {
        return FieldSpec.builder(clazz.java, name).constantField(value = value, format = format)
    }

    private fun FieldSpec.Builder.constantField(vararg value: Any?, format: String = "\$L"): FieldSpec {
        return addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
            .initializer(format, *value)
            .build()
    }

    fun build(
        versionInfo: VersionInfo,
        fileConfig: BuildInfoClassConfig,
        packageName: String = "build"
    ): JavaFile {
        return JavaFile.builder(
            packageName, TypeSpec.classBuilder("BuildInfo")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addFields(fileConfig.build(versionInfo))
                .build()
        ).build()
    }
}

