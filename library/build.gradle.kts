plugins {
    kotlin("jvm") version "1.9.20"
    `maven-publish`
    `kotlin-dsl`
    java
    `java-gradle-plugin`
    id("net.nemerosa.versioning")
}

group = "fe.plugin.library"
version = "testing"
//version = versioning.info.tag ?: versioning.info.full

kotlin {
    jvmToolchain(17)
}

gradlePlugin {
    plugins {
        create("library") {
            id = "com.gitlab.grrfe.common-gradle-plugin"
            implementationClass = "fe.plugin.library.LibraryPlugin"
        }
    }
}

dependencies {
    implementation(project(":common"))
    api("net.nemerosa.versioning:net.nemerosa.versioning.gradle.plugin:3.0.0")
    api("com.github.johnrengelman.shadow:com.github.johnrengelman.shadow.gradle.plugin:8.1.1")
    api("org.jetbrains.kotlin.jvm:org.jetbrains.kotlin.jvm.gradle.plugin:1.9.20")
    api("de.fayard.refreshVersions:refreshVersions-core:0.60.1")
}

tasks.named<Jar>("jar") {
    from(project(":common").sourceSets.main.get().output)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

tasks.withType<GenerateModuleMetadata> {
    enabled = false
}
