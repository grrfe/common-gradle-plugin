package fe.plugin.library

import org.gradle.api.Action
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.attributes.Attribute

class BundleConfigurationScope {
    var minimize = false

    companion object {
        val attribute = Attribute.of("bundle.minimize", Boolean::class.javaObjectType)
    }
}

fun DependencyHandler.bundle(
    dependencyNotation: String,
    configuration: Action<BundleConfigurationScope>
): ExternalModuleDependency {
    val dependency = add("bundle", dependencyNotation) as ExternalModuleDependency
    val scope = BundleConfigurationScope()
    configuration.execute(scope)

    dependency.attributes {
        attribute(BundleConfigurationScope.attribute, scope.minimize)
    }

    return dependency
}
