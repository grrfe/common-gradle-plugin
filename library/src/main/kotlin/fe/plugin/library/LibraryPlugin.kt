package fe.plugin.library

import com.github.jengelman.gradle.plugins.shadow.ShadowExtension
import com.github.jengelman.gradle.plugins.shadow.ShadowJavaPlugin
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import fe.plugin.common.BasePlugin
import fe.plugin.common.extension.add
import fe.plugin.common.extension.getAttribute
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.attributes.Attribute
import org.gradle.api.plugins.JavaLibraryPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin
import org.gradle.kotlin.dsl.*
import java.util.*

class LibraryPlugin : BasePlugin<LibraryConfig, Pair<Configuration, Configuration>>(
    "library", LibraryConfig::class, setOf(JavaLibraryPlugin::class, MavenPublishPlugin::class)
) {
    companion object {
        val PUBLISH_TASK_NAME = buildString {
            append("publish")
            append(ShadowJavaPlugin.SHADOW_JAR_TASK_NAME.replaceFirstChar {
                if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString()
            })
            append("PublicationToMavenLocal")
        }
    }

    override fun applyImpl(project: Project, config: LibraryConfig): Pair<Configuration, Configuration> {
        val implementation by project.configurations
        val relocate by project.configurations.creating {
            implementation.extendsFrom(this)
        }

        val bundle by project.configurations.creating {
            implementation.extendsFrom(this)
        }

        return Pair(relocate, bundle)
    }

    override fun afterEvaluate(project: Project, config: LibraryConfig, input: Pair<Configuration, Configuration>) {
        val publishing = project.extensions.getByType<PublishingExtension>()
        val shadowExt = project.extensions.getByType<ShadowExtension>()
        val java = project.extensions.getByType<JavaPluginExtension>()
        val shadow by project.configurations

        val (relocate, bundle) = input

        if (!config.disableSourceJar.get()) {
            java.withSourcesJar()
        }

        if (!config.disableJavaDocJar.get()) {
            java.withJavadocJar()
        }

        val shadowJarTask = project.tasks.named<ShadowJar>(ShadowJavaPlugin.SHADOW_JAR_TASK_NAME) {
            archiveClassifier.set("")
            exclude(JAR_EXCLUDES)

            if (config.kotlinStdLibConfig.ignoreWhenTransitive.get()) {
                KOTLIN_STD_LIB_DEPENDENCIES.forEach {
                    dependencyFilter.exclude(it.asSpec())
                }
            }

            val dependenciesToRelocate = relocate.resolvedConfiguration.firstLevelModuleDependencies

            val packages = Relocator(project).movePackagesToNameSpace(
                config.bundleConfig.namespace.get(), dependenciesToRelocate
            )

            packages.forEach { (from, to) ->
                relocate(from, to)
            }

            dependenciesToRelocate.forEach { relocateDep ->
                val suffix = relocateDep.getAttribute(RelocateConfiguration.attribute) ?: ""

                project.dependencies.add(shadow, config.bundleConfig.asBundledDependency(relocateDep, suffix))
                dependencyFilter.exclude(config.bundleConfig.asFilter(relocateDep, suffix))
            }

            bundle.copy().allDependencies.forEach { bundleDep ->
                val filter = dependencyFilter.dependency(bundleDep)
                dependencyFilter.include(filter)

                if (bundleDep.getAttribute(BundleConfigurationScope.attribute) == true) {
                    minimize { include(filter) }
                }
            }

            configurations = listOf(bundle, shadow)
        }

        project.tasks.named("jar") {
            enabled = false
        }

        publishing.publications.register<MavenPublication>(ShadowJavaPlugin.SHADOW_JAR_TASK_NAME) {
            shadowExt.component(this)

            val disallow = config.disallowGroupVersionArtifactOverride.get()

            groupId = project.findPropertyOrDefault(disallow, "_group", project.group)
            version = project.findPropertyOrDefault(disallow, "_version", project.version)
            artifactId = project.findPropertyOrDefault(disallow, "_artifact", project.name)
        }

        project.tasks.whenTaskAdded {
            if (name == PUBLISH_TASK_NAME) {
                dependsOn(shadowJarTask)
            }
        }
    }

    private fun Project.findPropertyOrDefault(
        disallowed: Boolean,
        propertyName: String,
        default: Any
    ): String {
        if (hasProperty(propertyName) && !disallowed) {
            return findProperty(propertyName).toString()
        }

        return default.toString()
    }
}
