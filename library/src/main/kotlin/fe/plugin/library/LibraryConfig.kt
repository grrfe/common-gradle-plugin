package fe.plugin.library


import fe.plugin.common.BaseConfig
import fe.plugin.common.PluginDependency
import fe.plugin.common.groovy.spec1
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.ResolvedDependency
import org.gradle.api.model.ObjectFactory
import org.gradle.api.specs.Spec
import org.gradle.kotlin.dsl.getByType
import javax.inject.Inject

open class LibraryConfig @Inject constructor(objectFactory: ObjectFactory) : BaseConfig(objectFactory) {
    val disallowGroupVersionArtifactOverride = property(false)
    val disableSourceJar = property(false)
    val disableJavaDocJar = property(false)

    val bundleConfig = newInstance<BundleConfig>()

    val kotlinStdLibConfig = newInstance<KotlinStdLibConfig>()

    fun bundle(action: Action<BundleConfig>) {
        action.execute(bundleConfig)
    }

    fun kotlinStdLib(action: Action<KotlinStdLibConfig>) {
        action.execute(kotlinStdLibConfig)
    }

    companion object {
        fun Project.library(group: String, action: Action<LibraryConfig>): LibraryConfig {
            val config = extensions.getByType<LibraryConfig>()
            config.group.set(group)

            action.execute(config)
            return config
        }
    }
}

open class BundleConfig @Inject constructor(objectFactory: ObjectFactory) : BaseConfig(objectFactory) {
    val moduleGroup = property("com.gitlab.grrfe.bundled-dependencies")
    val namespace = property("__bundled.dependencies")

    fun asBundledDependency(relocateDep: ResolvedDependency, suffix: String): PluginDependency.WithVersion {
        return PluginDependency.WithVersion(
            moduleGroup.get(),
            relocateDep.moduleName,
            relocateDep.moduleVersion + suffix
        )
    }

    fun asFilter(relocateDep: ResolvedDependency, suffix: String): Spec<ResolvedDependency> {
        return spec1 {
            (moduleGroup == this@BundleConfig.moduleGroup.get() || moduleGroup == relocateDep.moduleGroup)
                    && moduleName == relocateDep.moduleName && moduleVersion == (relocateDep.moduleVersion + suffix)
        }
    }
}

open class KotlinStdLibConfig @Inject constructor(objectFactory: ObjectFactory) : BaseConfig(objectFactory) {
    //    val includeRuntimeDependency = objectFactory.property(true)
    val ignoreWhenTransitive = property(true)
}
