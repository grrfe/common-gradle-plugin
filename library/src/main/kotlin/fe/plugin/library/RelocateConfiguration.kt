package fe.plugin.library

import org.gradle.api.Action
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.attributes.Attribute

class RelocateConfiguration {
    var removeSuffix: String? = null

    companion object {
        val attribute = Attribute.of("relocate.removeSuffix", String::class.javaObjectType)
    }
}

fun DependencyHandler.relocate(
    dependencyNotation: String,
    configuration: Action<RelocateConfiguration>
): ExternalModuleDependency {
    val scope = RelocateConfiguration()
    configuration.execute(scope)

    val bundleNotation = removeSuffix(dependencyNotation, scope.removeSuffix)
    val dependency = add("bundle", bundleNotation) as ExternalModuleDependency

    if (scope.removeSuffix != null) {
        dependency.attributes {
            attribute(RelocateConfiguration.attribute, scope.removeSuffix!!)
        }
    }

    return dependency
}

private fun removeSuffix(notation: String, suffix: String?): String {
    if (suffix == null) return notation
    val idx = notation.indexOf(suffix).takeIf { it > -1 } ?: return notation

    return notation.substring(0, idx)
}
