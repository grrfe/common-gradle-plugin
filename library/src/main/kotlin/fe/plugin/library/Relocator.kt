package fe.plugin.library

import org.gradle.api.Project
import org.gradle.api.artifacts.ResolvedDependency
import org.gradle.api.file.FileVisitDetails
import org.gradle.api.file.FileVisitor

class Relocator(private val project: Project) {
    fun movePackagesToNameSpace(
        namespace: String,
        resolvedDependencies: Set<ResolvedDependency>
    ): Map<String, String> {
        val pkgs = mutableMapOf<String, String>()

        val dependencies = resolvedDependencies.flatMap { dependency ->
            val visitor = Visitor(namespace, dependency, pkgs)
            dependency.moduleArtifacts.map { artifact -> visitor to artifact }
        }

        dependencies.forEach { (visitor, artifact) ->
            project.zipTree(artifact.file).visit(visitor)
        }

        return pkgs
    }


    private class Visitor(
        private val mergePackage: String,
        private val dependency: ResolvedDependency,
        private val packages: MutableMap<String, String>
    ) : FileVisitor {
        override fun visitDir(dirDetails: FileVisitDetails) {}

        override fun visitFile(fileDetails: FileVisitDetails) {
            val parentDir = fileDetails.toParentDir() ?: return

            val newPkg = listOf(
                mergePackage,
                fix(dependency.toPackage()),
                parentDir
            ).joinToString(".")

            packages[parentDir] = newPkg
        }

        private fun FileVisitDetails.toParentDir(): String? {
            if (relativePath.segments[0] != "META-INF") {
                return relativePath.segments.let { it.take(it.size - 1) }.joinToString(".")
            }

            return null
        }

        private fun ResolvedDependency.toPackage(): List<String> {
            return listOf(moduleGroup, moduleName, moduleVersion.replace("-", "___")).flatMap {
                it.split(".")
            }
        }

        private fun fix(pkg: List<String>): String {
            return pkg.joinToString(".") { if (it[0].isDigit()) "_$it" else it }
        }
    }
}
